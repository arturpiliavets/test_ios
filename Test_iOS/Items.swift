//
//  Items.swift
//  Test_iOS
//
//  Created by Artur Pilavetz on 14.06.2021.
//

import Foundation
import RealmSwift

class Items: Object {
    @objc dynamic var login: String = ""
    @objc dynamic var id: Int = 0
    @objc dynamic var avatar_url: String  = ""
    @objc dynamic var url: String  = ""
    @objc dynamic var repos_url: String  = ""
    
    convenience init(apiStruct: ApiStruct) {
        self.init()
        
        self.login = apiStruct.login
        self.id = apiStruct.id
        self.avatar_url = apiStruct.avatar_url
        self.url = apiStruct.url
        self.repos_url = apiStruct.repos_url
    }
    
    override class func primaryKey() -> String? {
            return "login"
        }
}







//class CollectionNotificationExampleViewController: UITableViewController {
//    var notificationToken: NotificationToken?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        let realm = try! Realm()
//        let results = realm.objects(Dog.self)
//
//        // Observe collection notifications. Keep a strong
//        // reference to the notification token or the
//        // observation will stop.
//        notificationToken = results.observe { [weak self] (changes: RealmCollectionChange) in
//            guard let tableView = self?.tableView else { return }
//            switch changes {
//            case .initial:
//                // Results are now populated and can be accessed without blocking the UI
//                tableView.reloadData()
//            case .update(_, let deletions, let insertions, let modifications):
//                // Query results have changed, so apply them to the UITableView
//                tableView.performBatchUpdates({
//                    // Always apply updates in the following order: deletions, insertions, then modifications.
//                    // Handling insertions before deletions may result in unexpected behavior.
//                    tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
//                                         with: .automatic)
//                    tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
//                                         with: .automatic)
//                    tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
//                                         with: .automatic)
//                }, completion: { finished in
//                    // ...
//                })
//            case .error(let error):
//                // An error occurred while opening the Realm file on the background worker thread
//                fatalError("\(error)")
//            }
//        }
//    }
//}
