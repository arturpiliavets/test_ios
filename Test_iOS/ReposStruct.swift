//
//  ReposStruct.swift
//  Test_iOS
//
//  Created by Artur Pilavetz on 14.06.2021.
//

import Foundation

struct ReposStruct: Decodable {
    let name: String
}
