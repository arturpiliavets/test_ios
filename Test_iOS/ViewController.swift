//
//  ViewController.swift
//  Test_iOS
//
//  Created by Artur Pilavetz on 13.06.2021.
//

import RealmSwift
import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    let realm = try! Realm()
    
    var objects: Results<Items>?
    
    var token:  NotificationToken?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        objects = realm.objects(Items.self)
        token = objects?.observe { [weak self] _ in
            self?.tableView.reloadData()
        }
        self.parseData {
        }
        
        render()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    func render(){
        let people = realm.objects(Items.self)
        for person in people {
            let firstName = person.login
            let lastName = person.url
            let id = person.id
            let fullName = "\(firstName) \(lastName) \(id)"
            print(fullName)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = objects?[indexPath.row].login.capitalized
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetails", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailedViewController {
            destination.login = objects?[(tableView.indexPathForSelectedRow?.row)!].login
        }
    }
    
    
    
    func parseData(completed: @escaping () -> ()) {
        let url = URL(string: "https://api.github.com/users")
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    let fetchedNames = try JSONDecoder().decode([ApiStruct].self, from: data!)
                    let realm = try Realm()
//                    self.realm.beginTransaction()
//                    self.realm.beginWrite()
                    let users = fetchedNames.compactMap({ Items(apiStruct: $0)})
//                    realm?.add(users)
                    try! realm.write {
                        realm.delete(realm.objects(Items.self))
                        realm.add(users, update: .modified)
                        
                    }
                } catch let DecodingError.dataCorrupted(context) {
                        print("error context: ", context)
                    } catch let DecodingError.keyNotFound(key, context) {
                        print("Key '\(key)' not found:", context.debugDescription)
                        print("codingPath:", context.codingPath)
                    } catch let DecodingError.valueNotFound(value, context) {
                        print("Value '\(value)' not found:", context.debugDescription)
                        print("codingPath:", context.codingPath)
                    } catch let DecodingError.typeMismatch(type, context)  {
                        print("Type '\(type)' mismatch:", context.debugDescription)
                        print("codingPath:", context.codingPath)
                    } catch {
                        print("error: ", error)
                    }
            }
        }.resume()
        print("start")
        print(OperationQueue.current)
    }
}
