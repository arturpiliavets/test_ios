//
//  DetailedViewController.swift
//  Test_iOS
//
//  Created by Artur Pilavetz on 14.06.2021.
//

import RealmSwift
import UIKit

class DetailedViewController: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    
    @IBOutlet weak var reposButton: UIButton!
    
    var login:String?
    var fetchedNames = [ApiStruct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let obj = try! Realm().object(ofType: Items.self, forPrimaryKey: login)
        
        
        loginLabel.text = "Login: \(obj!.login)"
        idLabel.text  = "ID: \(obj!.id)" //String(user?.id)//"\(String(describing: user?.id))"
        urlLabel.text = "Repos URL: \(obj!.repos_url)"
        
        
        let imageURL = obj?.avatar_url
        let url = URL(string: imageURL!)
        avatarImageView.downloadImage(url: url!)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showRepos", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ReposViewController {
            let obj = try! Realm().object(ofType: Items.self, forPrimaryKey: login)
            
            destination.reposURL = obj!.repos_url
        }
    }
}

extension UIImageView {
    func downloadImage(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    
    func downloadImage(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadImage(url: url, contentMode: mode)
    }
}

