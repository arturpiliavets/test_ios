//
//  ApiStruct.swift
//  Test_iOS
//
//  Created by Artur Pilavetz on 14.06.2021.
//

import Foundation

struct ApiStruct: Decodable {
    let login: String
    let id: Int
    let avatar_url: String
    let url: String
    let repos_url: String
}
