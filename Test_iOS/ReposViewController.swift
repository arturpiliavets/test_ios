//
//  ReposViewController.swift
//  Test_iOS
//
//  Created by Artur Pilavetz on 14.06.2021.
//

import RealmSwift
import UIKit

class ReposViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    var fetchedRepos = [ReposStruct]()
    var objects: Results<Items>?
    var reposURL = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        parseData{
            self.tableView.reloadData()
        }
        
        print(reposURL)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedRepos.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = fetchedRepos[indexPath.row].name.capitalized
        return cell
    }
    
    
    func parseData(completed: @escaping () -> ()) {
        let url = URL(string: "\(reposURL)")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    self.fetchedRepos = try JSONDecoder().decode([ReposStruct].self, from: data!)
                    //                    print(self.fetchedNames)
                    
                    DispatchQueue.main.async {
                        completed()
                    }
                }catch {
                    print("JSON Error repos")
                }
            }
        }.resume()
    }
    
    
    
    
    
    
    
    
//    func parseData(completed: @escaping () -> ()) {
//        let url = URL(string: "https://api.github.com/users")
//
//        URLSession.shared.dataTask(with: url!) { (data, response, error) in
//            if error == nil {
//                do {
//                    let fetchedNames = try JSONDecoder().decode([ApiStruct].self, from: data!)
//                    let realm = try Realm()
////                    self.realm.beginTransaction()
////                    self.realm.beginWrite()
//                    let users = fetchedNames.compactMap({ Items(apiStruct: $0)})
////                    realm?.add(users)
//                    try! realm.write {
//                        realm.delete(realm.objects(Items.self))
//                        realm.add(users, update: .modified)
//
//                    }
//                } catch let DecodingError.dataCorrupted(context) {
//                        print("error context: ", context)
//                    } catch let DecodingError.keyNotFound(key, context) {
//                        print("Key '\(key)' not found:", context.debugDescription)
//                        print("codingPath:", context.codingPath)
//                    } catch let DecodingError.valueNotFound(value, context) {
//                        print("Value '\(value)' not found:", context.debugDescription)
//                        print("codingPath:", context.codingPath)
//                    } catch let DecodingError.typeMismatch(type, context)  {
//                        print("Type '\(type)' mismatch:", context.debugDescription)
//                        print("codingPath:", context.codingPath)
//                    } catch {
//                        print("error: ", error)
//                    }
//            }
//        }.resume()
//        print("start")
//        print(OperationQueue.current)
//    }
}
