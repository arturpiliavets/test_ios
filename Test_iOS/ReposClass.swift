//
//  ReposClass.swift
//  Test_iOS
//
//  Created by Artur Pilavetz on 26.06.2021.
//

import Foundation
import RealmSwift


class ReposClass: Object {
    @objc dynamic var name: String = ""
    override static func indexedProperties() -> [String] {
            return ["name"]
        }
}
